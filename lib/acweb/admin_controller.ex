defmodule AdminController do
  require Lager

  def login(:get, _, _query, _headers) do
    {:html, :login_dtl, []}
  end

  def login(:post, _, _query, _headers) do
    {:redirect, "/admin/dashboard"}
  end

  def dashboard(:get, _, _query, _headers) do
    {:html, :dashboard_dtl, [admin_menu: nil]}
  end

end
