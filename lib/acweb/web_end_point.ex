defmodule AcWeb.EndPoint do
  def start_link do
    port = Application.get_env(:acweb, :web_port) || 4080
    sse_routes = Application.get_env(:acweb, :sse_routes) || []
    dispatch = :cowboy_router.compile([
      {:_,
        sse_routes ++
        [
          {"/[...]", AcWeb.Web.HttpRouter, []}
        # {"/:token...", Web.HttpRouter, []}
        ]
      }
    ])
    {:ok, _} = :cowboy.start_http(:http, 100, [{:port, port}],
                                  [{:env, [{:dispatch, dispatch}]}])
  end

  def stop(_state) do
    :ok
  end
end


