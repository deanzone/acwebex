defmodule Lager do
  @config_level Application.get_env(:lager, :level) || :debug
  defp loggable?(level) do
    level_map = %{none: 0, emergency: 1, alert: 2, critical: 3, error: 4, warn: 5, notice: 6, info: 7, debug: 8}
    level_map[level] <= @config_level
  end

  defmacro emergency(format, args, meta), do: do_logl(:emergency, format, args, meta)
  defmacro emergency(format, args), do: do_logl(:emergency, format, args, [])
  defmacro emergency(format), do: do_logl(:emergency, format, [], [])

  defmacro alert(format, args, meta), do: do_logl(:alert, format, args, meta)
  defmacro alert(format, args), do: do_logl(:alert, format, args, [])
  defmacro alert(format), do: do_logl(:alert, format, [], [])

  defmacro critical(format, args, meta), do: do_logl(:critical, format, args, meta)
  defmacro critical(format, args), do: do_logl(:critical, format, args, [])
  defmacro critical(format), do: do_logl(:critical, format, [], [])

  defmacro error(format, args, meta), do: do_logl(:error, format, args, meta)
  defmacro error(format, args), do: do_logl(:error, format, args, [])
  defmacro error(format), do: do_logl(:error, format, [], [])

  defmacro warn(format, args, meta), do: do_logl(:warning, format, args, meta)
  defmacro warn(format, args), do: do_logl(:warning, format, args, [])
  defmacro warn(format), do: do_logl(:warning, format, [], [])

  defmacro notice(format, args, meta), do: do_logl(:notice, format, args, meta)
  defmacro notice(format, args), do: do_logl(:notice, format, args, [])
  defmacro notice(format), do: do_logl(:notice, format, [], [])

  defmacro info(format, args, meta), do: do_logl(:info, format, args, meta)
  defmacro info(format, args), do: do_logl(:info, format, args, [])
  defmacro info(format), do: do_logl(:info, format, [], [])

  defmacro debug(format, args, meta), do: do_logl(:debug, format, args, meta)
  defmacro debug(format, args), do: do_logl(:debug, format, args, [])
  defmacro debug(format), do: do_logl(:debug, format, [], [])

  defp do_logl(level, format, args, meta) do
    if loggable?(level) do
      quote do
        :lager.log(unquote(level), unquote(meta), unquote(format),  unquote(args))
        #IO.puts("#{unquote(inspect level)}, #{unquote(inspect format)}, #{unquote(inspect args)}")
      end
    else
      quote do
        if (false) do
          unquote(args)
        end
      end
    end
  end
end

