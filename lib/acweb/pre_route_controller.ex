defmodule PreRouteController do
  require Lager

  def check_logged_in(req, _headers) do
    {path, _} = :cowboy_req.path(req)
    Lager.debug("Authent check for ~p", [path])
    {true, nil}
  end
end
