defmodule AcWeb.HttpRouter do
  require Lager
  @behaviour :cowboy_http_handler
  @app          :acweb
  @html_header  [{'Content-Type', "text/html"}]
  @json_header  [{'Content-Type', "application/json"}]
  @xml_header   [{'Content-Type', "application/xml"}]
  @text_header  [{'Content-Type', "text/text"}]
  @options_header [{'Access-Control-Allow-Origin', '*'},
                   {'Access-Control-Allow-Headers', 'Content-Type,
                      Access-Control-Allow-Headers, Authorization, X-Requested-With'},
                   {'Allow', 'HEAD,GET,PUT,DELETE,OPTIONS'}]

  def content_hdr(ctype), do: [{'Content-Type', ctype}] |> get_hdr
  def get_hdr(content_hdr), do: content_hdr ++ @options_header

  def http_url, do: Application.get_env(@app, :http_url) || "about:blank"

  def init({_any, :http}, req, _opts) do
    auth_mods = (Application.get_env(@app, :auth_modules) || [])
                |> Enum.map(fn({name, module, method}) -> {name, {module, method}} end)
                |> Map.new
    {:ok, req, %{auth_mods: auth_mods}}
  end

  def handle(req, state = %{auth_mods: auth_mods}) do
    {path, _} = :cowboy_req.path(req)
    {m, _} = :cowboy_req.method(req)
    headers = get_headers(req)

    qs_val_map = :cowboy_req.qs_vals(req)
    |> (fn({qs_vals, _}) -> get_body(req, Map.new(qs_vals)) end).()

    method = m |> String.downcase |> String.to_atom

    log_debug("#{inspect match_route(method, path)} #{inspect qs_val_map}")
    {:ok, req2} =
      try do
        case match_route(method, path) do
          {:options, _, _, _} -> :cowboy_req.reply(200, @options_header, "", req)
          {:notfound, _, _} -> render_notfound(req, headers, method, path, qs_val_map)
          {httpm, module, func, args, opts} ->
            case call_controller_module(module, func, [httpm, args, qs_val_map, headers], opts, auth_mods, req, headers)  do
              {:text, body} -> :cowboy_req.reply(200, get_hdr(@text_header), body, req)
              {:html, template, vars} when is_atom(template) -> build_html(template, vars, req)
              {:json, body} -> :cowboy_req.reply(200, get_hdr(@json_header), body |> Poison.encode!, req)
              {:xml, body} -> :cowboy_req.reply(200, get_hdr(@xml_header), body, req)
              {:binary, hdr, data} -> :cowboy_req.reply(200, content_hdr(hdr), data, req)
              {:moved, url} -> :cowboy_req.reply(301, @html_header ++ [{"Location", url}], "", req)
              {:redirect, url} -> :cowboy_req.reply(302, @html_header ++ [{"Location", url}], "", req)
              {:gone, _} -> :cowboy_req.reply(410, @html_header, req)
              {:error, 401} -> render_error(req, headers, 401, "Restricted", "This resource needs an authenticated session!")
              {:error, 403} -> render_error(req, headers, 403, "Unauthorized", "You are not authorized for this resource!")
              {:error, code} -> render_error(req, headers, code, "Error #{code}", "Server returned #{code}, contact admin!")
              {status_code, content_header, body} when is_atom(content_header) ->
                              :cowboy_req.reply(status_code, get_content_header(content_header) |> get_hdr, body, req)
                        _ -> render_notfound(req, headers, method, path, qs_val_map)
            end
        end
      rescue
        e ->
          Lager.error(inspect e)
          Lager.error(inspect System.stacktrace)
          render_server_error(req, headers)
      end
    {:ok, req2, state}
  end

  def info(:eof, req, state) do
    {:ok, req, state}
  end

  def info(msg, req, state) do
    IO.puts "bad msg => #{inspect msg}"
    {:ok, req, state}
  end

  def terminate(_reason, _req, _state) do
    # IO.puts "Terminate Message Received"
    :ok
  end

  defp call_controller_module(m, f, a, auths, auth_mods, req, headers) do
    case auth_match(auths, auth_mods, req, headers) do
      {true, _} -> apply(m, f, a)
      {false, code} -> {:error, code}
      error -> Lager.error "#{error} is not a valid return from pre_route controller. Has to be {true or false, http_code or nil}"
               {:error, 500}
    end
  end
  defp auth_match([], _, _, _), do: {true, nil}
  defp auth_match(auths, auth_mods, req, headers) do
    Enum.reduce_while(auths, {true, 500}, fn(auth_name, acc) ->
      Lager.debug "auth name --> #{auth_name} #{inspect auth_mods}"
      case auth_mods[auth_name] do
        nil -> Lager.error "Missing Route Check module #{auth_name} - Routes don't work as intended"
               {:cont, acc}
        {m, f} -> Lager.debug "Calling pre route mod #{m} #{f}"
                  case apply(m, f, [req, headers]) do
                    {false, code} -> {:halt, {false, code}}
                    {true, _} -> {:cont, acc}
                  end
      end
    end)
  end

  #<<<<#########  Render Error Messages ############
  defp render_error(req, headers, code, msg, long_msg) do
    {header, body} = get_mimed_error_body(headers, code, msg, long_msg)
    :cowboy_req.reply(500, header, body, req)
  end
  defp render_server_error(req, headers) do
    render_error(req, headers, 500, "Server Error",
                                          "The server encountered an error processing your request.")
  end
  defp get_error_text(:html, 500, _short_msg, _msg) do
    {:ok, body} = :error500_dtl.render([])
    body
  end
  defp get_error_text(:html, 404, _short_msg, _msg) do
    {:ok, body} = :page404_dtl.render([{:meta, [
                                      {:title, "Page Not Found - Is this what you are looking for"},
                                      {:desc, "Page Not Found"}]}])
    body
  end
  defp get_error_text(:html, code, short_msg, msg) do
    basic_html_page(short_msg, "<h1>#{code} -  #{msg}</h1>")
  end
  def render_notfound(req, headers, method, path, qs_vals) do
    log_debug("Route Not Found - #{inspect method}, #{inspect path}, #{inspect qs_vals}")
    {header, body} = get_mimed_error_body(headers, 404, "Not Found",
                          "Request cannot be processed. Resource not available or you sent an invalid request.")
    :cowboy_req.reply(404, header, body, req)
  end

  defp get_mimed_error_body(headers, code, short_msg, msg) do
    mime_types = [{"*/*", :html}, {"text/html", :html}, {"application/json", :json}, {"application/xml", :xml},
                    {"text/plain", :text}, {"text/csv", :text}]
    mimes = Map.get(headers, "accept", "*/*") |> String.split(",")
    case Enum.find(mime_types, fn({k, _t}) -> Enum.find(mimes, &(&1 == k)) end) do
      {_mime, :html} -> {@html_header, get_error_text(:html, code, short_msg, msg)}
      {_mime, :xml} -> {@xml_header, ~s(<?xml version="1.0" encoding="UTF-8" ?><error>
                          <code>#{code}</code>
                          <short_message>#{short_msg}</short_message><message>#{msg}</message>
                          </error>")}
      {_mime, :json} -> {@json_header, ~s({"error": {"code": "#{code}", "short_message": "#{short_msg}",
                           "message": "#{msg}"}})}
                   _ -> {@text_header, ~s(code: #{code}\n short message: #{short_msg}\n message: #{msg} \n)}

    end
  end
  ############ Render Error Messages >>>>>>>>>>#
  defp process_auth_headers(headers = %{"authorization" => auth}) do
    case String.split(auth, "Basic ") do
      [_,tk] ->
        case :base64.decode(tk) |> String.split(":") do
          [user, pw] -> Map.put(headers, "basic-auth", {user, pw})
          _ -> headers
        end
        _ -> headers
    end
  end
  defp process_auth_headers(headers), do: headers


  defp get_headers(req) do
    {headers_kw, _} = :cowboy_req.headers(req)
    {{remote_ip, remote_port}, _} = :cowboy_req.peer(req)
    headers = Map.new(headers_kw)
    # log_debug("Remote_ip --> #{inspect remote_ip}")
    # log_debug("X-forwarde-for --> #{inspect Map.get(headers, "x-forwarded-for")}")
    case Map.get(headers, "x-forwarded-for") do
      nil -> Map.merge(headers, %{"remote-ip" => remote_ip, "remote-port" => remote_port})
      ip  -> Map.merge(headers, %{"remote-ip" => convert_to_ip(ip), "remote-port" => remote_port})
    end
    |> process_auth_headers
  end

  defp get_content_header(header) do
    case header do
      :json -> @json_header
      :html -> @html_header
      :xml  -> @xml_header
          _ -> @text_header
    end
  end

  defp convert_to_ip(ipStr) do
    try do
      ipStr |> String.split(".") |> Enum.map(&String.to_integer/1) |> List.to_tuple
    rescue
     _ -> {0,0,0,0}
    end
  end


  # cowboy has 8MB body length limit by default
  # if more data available body will return {:more, body, req2}
  # hence the code below will fail.
  def get_body(req, qs_vals) do
    case :cowboy_req.has_body(req) do
      true ->
        case :cowboy_req.header("content-type", req) do
          {x, _} when x == "application/json;charset=UTF-8" or x == "application/json" ->
            {:ok, data, _} = :cowboy_req.body(req)
            Map.merge(qs_vals, data |> Poison.decode!)
          {"application/x-www-form-urlencoded", _} ->
            {:ok, body_vals, _} = :cowboy_req.body_qs(req)
            Map.merge(qs_vals, Map.new(body_vals))
          _ ->
            {:ok, body, _} = :cowboy_req.body(req)
            Map.put(qs_vals, :raw_body, body)
        end
      false ->
        qs_vals
    end
  end

  #def get_headers(req) do
    #["accept", "content-length", "content-type", "cookie", "x-forwarded-for"]
  #end
  def match_route(:options, _path) do
    {:options, nil, nil, nil}
  end

  def match_route(http_method, path) do
    result =
      Application.get_env(@app, :routes)
      |> Enum.find_value(fn(route_part) ->
        route_part_match(http_method, path, route_part)
      end)
    case result do
      nil -> {:notfound, nil, nil}
      {_mm, module, func, args, opts} -> {http_method, module, func, args, opts}
    end
  end
  def route_part_match(http_method, path, route_part) do
    case route_part do
      {m, r, c, f, o} when m == http_method or m == :any -> {m, r, c, f, o}
      {m, r, c, f} when m  == http_method or m == :any -> {m, r, c, f, []}
      _ -> nil
    end
    |> path_regex_match(path)
  end
  def path_regex_match(nil, _path), do: nil
  def path_regex_match({m, regex, ctrl, func, opt}, path) do
    case Regex.run(Regex.compile!(regex, [:caseless]), path) do
      nil -> nil
      [_ | result] -> {m, ctrl, func, result, opt}
    end
  end

  defp build_html(template, vars, req) do
    new_vars = add_default_vars(vars, req)
    #IO.puts(inspect new_vars)
    {:ok, body} = template.render(new_vars)
    :cowboy_req.reply(200, @html_header, body, req)
  end

  defp add_default_vars(vars, req) do
    {path, _} = :cowboy_req.path(req)
    {headers, _} = :cowboy_req.headers(req)
    header_map = Map.new(headers)
    Keyword.merge(vars, [current_url: path, is_mobile: is_mobile?(header_map),
    http_host: Map.get(header_map, "host"), http_url: http_url] ++
    (Application.get_env(@app, :template_vars) || []))
  end

  defp is_mobile?(headers) do
    user_agent = Map.get(headers, "user-agent", "desktop")
    Regex.match?(~r/iPad|iPhone|Android/, user_agent)
  end

  defp basic_html_page(title, body) do
    "<html><head><title>#{title}</title></head><body>#{body}</body></html>"
  end
  defp log_debug(msg) do
    Lager.debug(msg)
  end
end
