use Mix.Config

config :acweb,
  auth_modules: [
    {:logged_in, PreRouteController, :check_logged_in}
  ],  # send req object, and should return true for the routes to succeed
  routes:
      [
        {:get, "^/local/health-check", AdminController, :health_check},
        #{:get, "^/$", Controllers.DefaultController, :home},

        ## admin routes ##
        {:any, "^/admin/login$", AdminController, :login},
        #{:get, "^/admin/dashboard$", Controllers.AdminController, :dashboard, [:logged_in]},
      ]
