# Acweb

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `acweb` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:acweb, "~> 0.1.0"}]
    end
    ```

  2. Ensure `acweb` is started before your application:

    ```elixir
    def application do
      [applications: [:acweb]]
    end
    ```

