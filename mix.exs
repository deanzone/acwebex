defmodule Acweb.Mixfile do
  use Mix.Project

  def project do
    [app: :acweb,
     version: "0.1.0",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     compilers: [:erlydtl] ++ Mix.compilers,
     erlydtl_options: [
       source: "views/templates",
       report: true,
       doc_root: "views/templates",
       debug_root: "views/debug",
       debug_compiler: false,
       force_recompile: true,
       #libraries: [{:custom_filters, :dtl_custom_filters}],
       #default_libraries: [:custom_filters],
       compiler_options: [] # [:debug_info]
     ], deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:erlydtl, git: "https://github.com/erlydtl/erlydtl.git", tag: "master"},
      {:erlydtl_c, git: "git@bitbucket.org:deanzone/erlydtl_c.git", tag: "0.0.1"},
      {:poison,     "~> 2.2.0"},
      {:cowboy,     "~> 1.0.1"},
      {:lager,      github: "erlang-lager/lager", tag: "3.5.1"},
    ]
  end
end
